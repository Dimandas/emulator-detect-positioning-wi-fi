﻿
namespace Emulator_Detect_Positioning_Wi_Fi.Source.Menu
{
    partial class Add_sensor_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.X_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Y_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Z_textBox = new System.Windows.Forms.TextBox();
            this.calculation_method = new System.Windows.Forms.ComboBox();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Z_enabled = new System.Windows.Forms.CheckBox();
            this.Sensor_enabled = new System.Windows.Forms.CheckBox();
            this.input_value_1 = new System.Windows.Forms.TextBox();
            this.input_caption_1 = new System.Windows.Forms.Label();
            this.input_caption_2 = new System.Windows.Forms.Label();
            this.input_value_2 = new System.Windows.Forms.TextBox();
            this.input_value_4 = new System.Windows.Forms.TextBox();
            this.input_caption_4 = new System.Windows.Forms.Label();
            this.input_caption_3 = new System.Windows.Forms.Label();
            this.input_value_3 = new System.Windows.Forms.TextBox();
            this.input_value_6 = new System.Windows.Forms.TextBox();
            this.input_caption_6 = new System.Windows.Forms.Label();
            this.input_caption_5 = new System.Windows.Forms.Label();
            this.input_value_5 = new System.Windows.Forms.TextBox();
            this.input_value_8 = new System.Windows.Forms.TextBox();
            this.input_caption_8 = new System.Windows.Forms.Label();
            this.input_caption_7 = new System.Windows.Forms.Label();
            this.input_value_7 = new System.Windows.Forms.TextBox();
            this.input_value_10 = new System.Windows.Forms.TextBox();
            this.input_caption_10 = new System.Windows.Forms.Label();
            this.input_caption_9 = new System.Windows.Forms.Label();
            this.input_value_9 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(0, 417);
            this.add.Margin = new System.Windows.Forms.Padding(4);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(445, 48);
            this.add.TabIndex = 0;
            this.add.Text = "Добавить";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "X: ";
            // 
            // X_textBox
            // 
            this.X_textBox.Location = new System.Drawing.Point(40, 62);
            this.X_textBox.Margin = new System.Windows.Forms.Padding(4);
            this.X_textBox.Name = "X_textBox";
            this.X_textBox.Size = new System.Drawing.Size(132, 22);
            this.X_textBox.TabIndex = 0;
            this.X_textBox.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Y: ";
            // 
            // Y_textBox
            // 
            this.Y_textBox.Location = new System.Drawing.Point(297, 62);
            this.Y_textBox.Margin = new System.Windows.Forms.Padding(4);
            this.Y_textBox.Name = "Y_textBox";
            this.Y_textBox.Size = new System.Drawing.Size(134, 22);
            this.Y_textBox.TabIndex = 4;
            this.Y_textBox.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Z: ";
            this.label3.Visible = false;
            // 
            // Z_textBox
            // 
            this.Z_textBox.Location = new System.Drawing.Point(159, 103);
            this.Z_textBox.Margin = new System.Windows.Forms.Padding(4);
            this.Z_textBox.Name = "Z_textBox";
            this.Z_textBox.Size = new System.Drawing.Size(132, 22);
            this.Z_textBox.TabIndex = 6;
            this.Z_textBox.Text = "0";
            this.Z_textBox.Visible = false;
            // 
            // calculation_method
            // 
            this.calculation_method.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.calculation_method.FormattingEnabled = true;
            this.calculation_method.Items.AddRange(new object[] {
            "Free-space path loss"});
            this.calculation_method.Location = new System.Drawing.Point(148, 148);
            this.calculation_method.Margin = new System.Windows.Forms.Padding(4);
            this.calculation_method.Name = "calculation_method";
            this.calculation_method.Size = new System.Drawing.Size(283, 24);
            this.calculation_method.TabIndex = 7;
            // 
            // name_textBox
            // 
            this.name_textBox.Location = new System.Drawing.Point(127, 23);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(132, 22);
            this.name_textBox.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Наименование: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Метод измерения: ";
            // 
            // Z_enabled
            // 
            this.Z_enabled.AutoSize = true;
            this.Z_enabled.Location = new System.Drawing.Point(296, 102);
            this.Z_enabled.Name = "Z_enabled";
            this.Z_enabled.Size = new System.Drawing.Size(129, 21);
            this.Z_enabled.TabIndex = 11;
            this.Z_enabled.Text = "Задействовать";
            this.Z_enabled.UseVisualStyleBackColor = true;
            this.Z_enabled.Visible = false;
            // 
            // Sensor_enabled
            // 
            this.Sensor_enabled.AutoSize = true;
            this.Sensor_enabled.Checked = true;
            this.Sensor_enabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Sensor_enabled.Location = new System.Drawing.Point(296, 23);
            this.Sensor_enabled.Name = "Sensor_enabled";
            this.Sensor_enabled.Size = new System.Drawing.Size(135, 21);
            this.Sensor_enabled.TabIndex = 12;
            this.Sensor_enabled.Text = "Сенсор активен";
            this.Sensor_enabled.UseVisualStyleBackColor = true;
            // 
            // input_value_1
            // 
            this.input_value_1.Location = new System.Drawing.Point(107, 193);
            this.input_value_1.Name = "input_value_1";
            this.input_value_1.Size = new System.Drawing.Size(100, 22);
            this.input_value_1.TabIndex = 13;
            this.input_value_1.Visible = false;
            // 
            // input_caption_1
            // 
            this.input_caption_1.AutoSize = true;
            this.input_caption_1.Location = new System.Drawing.Point(7, 196);
            this.input_caption_1.Name = "input_caption_1";
            this.input_caption_1.Size = new System.Drawing.Size(94, 17);
            this.input_caption_1.TabIndex = 14;
            this.input_caption_1.Text = "input_capt_1:";
            this.input_caption_1.Visible = false;
            // 
            // input_caption_2
            // 
            this.input_caption_2.AutoSize = true;
            this.input_caption_2.Location = new System.Drawing.Point(231, 194);
            this.input_caption_2.Name = "input_caption_2";
            this.input_caption_2.Size = new System.Drawing.Size(94, 17);
            this.input_caption_2.TabIndex = 15;
            this.input_caption_2.Text = "input_capt_2:";
            this.input_caption_2.Visible = false;
            // 
            // input_value_2
            // 
            this.input_value_2.Location = new System.Drawing.Point(331, 193);
            this.input_value_2.Name = "input_value_2";
            this.input_value_2.Size = new System.Drawing.Size(100, 22);
            this.input_value_2.TabIndex = 16;
            this.input_value_2.Visible = false;
            // 
            // input_value_4
            // 
            this.input_value_4.Location = new System.Drawing.Point(331, 230);
            this.input_value_4.Name = "input_value_4";
            this.input_value_4.Size = new System.Drawing.Size(100, 22);
            this.input_value_4.TabIndex = 20;
            this.input_value_4.Visible = false;
            // 
            // input_caption_4
            // 
            this.input_caption_4.AutoSize = true;
            this.input_caption_4.Location = new System.Drawing.Point(231, 231);
            this.input_caption_4.Name = "input_caption_4";
            this.input_caption_4.Size = new System.Drawing.Size(94, 17);
            this.input_caption_4.TabIndex = 19;
            this.input_caption_4.Text = "input_capt_4:";
            this.input_caption_4.Visible = false;
            // 
            // input_caption_3
            // 
            this.input_caption_3.AutoSize = true;
            this.input_caption_3.Location = new System.Drawing.Point(7, 233);
            this.input_caption_3.Name = "input_caption_3";
            this.input_caption_3.Size = new System.Drawing.Size(94, 17);
            this.input_caption_3.TabIndex = 18;
            this.input_caption_3.Text = "input_capt_3:";
            this.input_caption_3.Visible = false;
            // 
            // input_value_3
            // 
            this.input_value_3.Location = new System.Drawing.Point(107, 230);
            this.input_value_3.Name = "input_value_3";
            this.input_value_3.Size = new System.Drawing.Size(100, 22);
            this.input_value_3.TabIndex = 17;
            this.input_value_3.Visible = false;
            // 
            // input_value_6
            // 
            this.input_value_6.Location = new System.Drawing.Point(331, 270);
            this.input_value_6.Name = "input_value_6";
            this.input_value_6.Size = new System.Drawing.Size(100, 22);
            this.input_value_6.TabIndex = 24;
            this.input_value_6.Visible = false;
            // 
            // input_caption_6
            // 
            this.input_caption_6.AutoSize = true;
            this.input_caption_6.Location = new System.Drawing.Point(231, 271);
            this.input_caption_6.Name = "input_caption_6";
            this.input_caption_6.Size = new System.Drawing.Size(94, 17);
            this.input_caption_6.TabIndex = 23;
            this.input_caption_6.Text = "input_capt_6:";
            this.input_caption_6.Visible = false;
            // 
            // input_caption_5
            // 
            this.input_caption_5.AutoSize = true;
            this.input_caption_5.Location = new System.Drawing.Point(7, 273);
            this.input_caption_5.Name = "input_caption_5";
            this.input_caption_5.Size = new System.Drawing.Size(94, 17);
            this.input_caption_5.TabIndex = 22;
            this.input_caption_5.Text = "input_capt_5:";
            this.input_caption_5.Visible = false;
            // 
            // input_value_5
            // 
            this.input_value_5.Location = new System.Drawing.Point(107, 270);
            this.input_value_5.Name = "input_value_5";
            this.input_value_5.Size = new System.Drawing.Size(100, 22);
            this.input_value_5.TabIndex = 21;
            this.input_value_5.Visible = false;
            // 
            // input_value_8
            // 
            this.input_value_8.Location = new System.Drawing.Point(331, 308);
            this.input_value_8.Name = "input_value_8";
            this.input_value_8.Size = new System.Drawing.Size(100, 22);
            this.input_value_8.TabIndex = 28;
            this.input_value_8.Visible = false;
            // 
            // input_caption_8
            // 
            this.input_caption_8.AutoSize = true;
            this.input_caption_8.Location = new System.Drawing.Point(231, 309);
            this.input_caption_8.Name = "input_caption_8";
            this.input_caption_8.Size = new System.Drawing.Size(94, 17);
            this.input_caption_8.TabIndex = 27;
            this.input_caption_8.Text = "input_capt_8:";
            this.input_caption_8.Visible = false;
            // 
            // input_caption_7
            // 
            this.input_caption_7.AutoSize = true;
            this.input_caption_7.Location = new System.Drawing.Point(7, 311);
            this.input_caption_7.Name = "input_caption_7";
            this.input_caption_7.Size = new System.Drawing.Size(94, 17);
            this.input_caption_7.TabIndex = 26;
            this.input_caption_7.Text = "input_capt_7:";
            this.input_caption_7.Visible = false;
            // 
            // input_value_7
            // 
            this.input_value_7.Location = new System.Drawing.Point(107, 308);
            this.input_value_7.Name = "input_value_7";
            this.input_value_7.Size = new System.Drawing.Size(100, 22);
            this.input_value_7.TabIndex = 25;
            this.input_value_7.Visible = false;
            // 
            // input_value_10
            // 
            this.input_value_10.Location = new System.Drawing.Point(331, 347);
            this.input_value_10.Name = "input_value_10";
            this.input_value_10.Size = new System.Drawing.Size(100, 22);
            this.input_value_10.TabIndex = 32;
            this.input_value_10.Visible = false;
            // 
            // input_caption_10
            // 
            this.input_caption_10.AutoSize = true;
            this.input_caption_10.Location = new System.Drawing.Point(223, 350);
            this.input_caption_10.Name = "input_caption_10";
            this.input_caption_10.Size = new System.Drawing.Size(102, 17);
            this.input_caption_10.TabIndex = 31;
            this.input_caption_10.Text = "input_capt_10:";
            this.input_caption_10.Visible = false;
            // 
            // input_caption_9
            // 
            this.input_caption_9.AutoSize = true;
            this.input_caption_9.Location = new System.Drawing.Point(7, 350);
            this.input_caption_9.Name = "input_caption_9";
            this.input_caption_9.Size = new System.Drawing.Size(94, 17);
            this.input_caption_9.TabIndex = 30;
            this.input_caption_9.Text = "input_capt_9:";
            this.input_caption_9.Visible = false;
            // 
            // input_value_9
            // 
            this.input_value_9.Location = new System.Drawing.Point(107, 347);
            this.input_value_9.Name = "input_value_9";
            this.input_value_9.Size = new System.Drawing.Size(100, 22);
            this.input_value_9.TabIndex = 29;
            this.input_value_9.Visible = false;
            // 
            // Add_sensor_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 465);
            this.Controls.Add(this.input_value_10);
            this.Controls.Add(this.input_caption_10);
            this.Controls.Add(this.input_caption_9);
            this.Controls.Add(this.input_value_9);
            this.Controls.Add(this.input_value_8);
            this.Controls.Add(this.input_caption_8);
            this.Controls.Add(this.input_caption_7);
            this.Controls.Add(this.input_value_7);
            this.Controls.Add(this.input_value_6);
            this.Controls.Add(this.input_caption_6);
            this.Controls.Add(this.input_caption_5);
            this.Controls.Add(this.input_value_5);
            this.Controls.Add(this.input_value_4);
            this.Controls.Add(this.input_caption_4);
            this.Controls.Add(this.input_caption_3);
            this.Controls.Add(this.input_value_3);
            this.Controls.Add(this.input_value_2);
            this.Controls.Add(this.input_caption_2);
            this.Controls.Add(this.input_caption_1);
            this.Controls.Add(this.input_value_1);
            this.Controls.Add(this.Sensor_enabled);
            this.Controls.Add(this.Z_enabled);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.name_textBox);
            this.Controls.Add(this.calculation_method);
            this.Controls.Add(this.Z_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Y_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.X_textBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.add);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Add_sensor_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Добавить сенсор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox X_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Y_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Z_textBox;
        private System.Windows.Forms.ComboBox calculation_method;
        private System.Windows.Forms.TextBox name_textBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox Z_enabled;
        private System.Windows.Forms.CheckBox Sensor_enabled;
        private System.Windows.Forms.TextBox input_value_1;
        private System.Windows.Forms.Label input_caption_1;
        private System.Windows.Forms.Label input_caption_2;
        private System.Windows.Forms.TextBox input_value_2;
        private System.Windows.Forms.TextBox input_value_4;
        private System.Windows.Forms.Label input_caption_4;
        private System.Windows.Forms.Label input_caption_3;
        private System.Windows.Forms.TextBox input_value_3;
        private System.Windows.Forms.TextBox input_value_6;
        private System.Windows.Forms.Label input_caption_6;
        private System.Windows.Forms.Label input_caption_5;
        private System.Windows.Forms.TextBox input_value_5;
        private System.Windows.Forms.TextBox input_value_8;
        private System.Windows.Forms.Label input_caption_8;
        private System.Windows.Forms.Label input_caption_7;
        private System.Windows.Forms.TextBox input_value_7;
        private System.Windows.Forms.TextBox input_value_10;
        private System.Windows.Forms.Label input_caption_10;
        private System.Windows.Forms.Label input_caption_9;
        private System.Windows.Forms.TextBox input_value_9;
    }
}