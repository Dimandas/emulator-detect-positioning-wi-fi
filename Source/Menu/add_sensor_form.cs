﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Emulator_Detect_Positioning_Wi_Fi.Source.Menu
{
    using Emulator_Detect_Positioning_Wi_Fi.Source.Trilateration;
    public partial class Add_sensor_form : Form
    {
        List<Sensor> Sensors = new List<Sensor> { };

        public Add_sensor_form(ref List<Sensor> _sensors)
        {
            InitializeComponent();
            calculation_method.SelectedIndex = 0;

            Sensors = _sensors;

            name_textBox.Text = "Сенсор " + (Sensors.Count + 1).ToString();
        }

        private void add_Click(object sender, EventArgs e)
        {
            Sensors.Add(new Sensor(Convert.ToDouble(X_textBox.Text), Convert.ToDouble(Y_textBox.Text), calculation_method.SelectedIndex, name_textBox.Text));

            this.DialogResult = DialogResult.OK;
        }
    }
}