﻿using System;
using System.Drawing;

using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using OxyPlot.Annotations;
using OxyPlot.WindowsForms;


namespace Emulator_Detect_Positioning_Wi_Fi.Source.Menu
{
    public class OxyChart
    {
        public readonly PlotView chart = null;
        public readonly PlotModel oxyPlotModel = new PlotModel();
        public readonly PlotController oxyPlotController = new PlotController();

        public OxyChart(ref PlotView view)
        {
            chart = view;

            oxyPlotModel.PlotType = PlotType.Cartesian;
            oxyPlotModel.Background = OxyColor.FromRgb(SystemColors.Control.R, SystemColors.Control.G, SystemColors.Control.B);
            chart.Model = oxyPlotModel;

            //oxyPlotController.UnbindAll();
            chart.Controller = oxyPlotController;

            DrawXAxis();
            DrawYAxis();

            Update();
        }

        public void DrawCircle(double x, double y, double r, OxyColor color, double accuracy = 80)
        {
            double angle = 0;
            double dx = 0;
            double dy = 0;

            LineSeries points = new LineSeries();
            points.Color = color;

            for (int i = 0; i <= accuracy; i++)
            {
                angle = 2 * Math.PI * i / accuracy;
                dx = r * Math.Cos(angle);
                dy = r * Math.Sin(angle);

                points.Points.Add(new DataPoint(x + dx, y + dy));
            }

            oxyPlotModel.Series.Add(points);

            Update();
        }

        public void DrawLine(double x1, double y1, double x2, double y2, OxyColor color)
        {
            LineSeries points = new LineSeries();
            points.Color = color;

            points.Points.Add(new DataPoint(x1, y1));
            points.Points.Add(new DataPoint(x2, y2));

            oxyPlotModel.Series.Add(points);

            Update();
        }

        public void DrawMark(double x, double y, OxyColor color)
        {
            LineSeries points = new LineSeries();
            points.Color = color;

            points.Points.Add(new DataPoint(x, y));

            oxyPlotModel.Series.Add(points);

            Update();
        }

        public void DrawSquare(double x, double y, OxyColor color, double breadth = 1)
        {
            LineSeries points = new LineSeries();
            points.Color = color;

            points.Points.Add(new DataPoint(x - breadth, y + breadth));
            points.Points.Add(new DataPoint(x - breadth, y - breadth));
            points.Points.Add(new DataPoint(x + breadth, y - breadth));
            points.Points.Add(new DataPoint(x + breadth, y + breadth));
            points.Points.Add(new DataPoint(x - breadth, y + breadth));

            oxyPlotModel.Series.Add(points);

            Update();
        }

        public void DrawPoint(double x, double y, OxyColor color, bool drawMark = true, double sizePoint = -1)
        {
            PointAnnotation points = new PointAnnotation();
            points.X = x;
            points.Y = y;

            if (sizePoint > 0)
            {
                points.Size = sizePoint;
            }
            points.Fill = color;

            if (drawMark == true)
            {
                DrawMark(x, y, color);
            }

            oxyPlotModel.Annotations.Add(points);

            Update();
        }

        public void DrawPointText(double x, double y, OxyColor colorFill, string text, OxyColor colorText, bool drawMark = true, double sizePoint = -1, double sizeText = -1)
        {
            PointAnnotation points = new PointAnnotation();
            points.X = x;
            points.Y = y;
            points.Text = text;

            if (sizeText > 0)
            {
                points.FontSize = sizeText;
            }
            points.TextColor = colorText;

            if (sizePoint > 0)
            {
                points.Size = sizePoint;
            }
            points.Fill = colorFill;

            if (drawMark == true)
            {
                DrawMark(x, y, colorFill);
            }

            oxyPlotModel.Annotations.Add(points);

            Update();
        }

        public void DeleteAllShapes()
        {
            oxyPlotModel.Series.Clear();
            oxyPlotModel.Annotations.Clear();

            Update();
        }

        public void DeleteAllAnnotations()
        {
            oxyPlotModel.Annotations.Clear();

            Update();
        }

        public void DeleteAllAxis()
        {
            oxyPlotModel.Axes.Clear();

            Update();
        }

        public void DrawXAxis()
        {
            LinearAxis line = new LinearAxis();
            line.Position = AxisPosition.Bottom;
            line.ExtraGridlines = new double[] { 0 };
            line.ExtraGridlineThickness = 1;
            line.ExtraGridlineColor = OxyColors.Black;

            oxyPlotModel.Axes.Add(line);

            Update();
        }

        public void DrawYAxis()
        {
            LinearAxis line = new LinearAxis();
            line.Position = AxisPosition.Left;
            line.ExtraGridlines = new double[] { 0 };
            line.ExtraGridlineThickness = 1;
            line.ExtraGridlineColor = OxyColors.Black;

            oxyPlotModel.Axes.Add(line);

            Update();
        }

        public void Update(bool updateData = true)
        {
            chart.InvalidatePlot(updateData);
        }
    }
}