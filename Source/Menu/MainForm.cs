﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

using OxyPlot;


namespace Emulator_Detect_Positioning_Wi_Fi
{
    using Emulator_Detect_Positioning_Wi_Fi.Source.Trilateration;
    using Emulator_Detect_Positioning_Wi_Fi.Source.Menu;

    public partial class MainForm : Form
    {
        public static double ReverseCalculateDistanceAttenuationFSPL(double distance, double freq)
        {
            double exp1 = Math.Log10(distance);
            double answer = 20 * exp1 + 20 * Math.Log10(freq / 1000) - 27.55;

            return answer;
        }

        public void UpdateFormState()
        {
            if (Sensors.Count > 0)
            {
                all_cleartoolStripMenuItem.Enabled = true;
            }
            else
            {
                all_cleartoolStripMenuItem.Enabled = false;
            }

            if (Sensors.Count > 1)
            {
                calcStatusLabel.Enabled = true;
            }
            else
            {
                calcStatusLabel.Enabled = false;
            }

            if (display_current_coordinatesStripMenuItem.Checked == true)
            {
                this.chart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart_MouseMove);
                label_X_statusLabel.Visible = true;
                status_X.Visible = true;
                label_Y_statusLabel.Visible = true;
                status_Y.Visible = true;
            }
            else
            {
                this.chart.MouseMove += null;
                label_X_statusLabel.Visible = false;
                status_X.Visible = false;
                label_Y_statusLabel.Visible = false;
                status_Y.Visible = false;
            }
        }

        public void RedrawAllChart()
        {
            oxyChart.DeleteAllShapes();
            for (int i = 0; i < Sensors.Count; i++)
            {
                oxyChart.DrawCircle(Sensors[i].X, Sensors[i].Y, Sensors[i].distance, OxyColors.Black);
                oxyChart.DrawPointText(Sensors[i].X, Sensors[i].Y, OxyColors.Black, Sensors[i].name, OxyColors.Black);
            }
        }

        OxyChart oxyChart;
        List<Sensor> Sensors = new List<Sensor>() { };

        public MainForm()
        {
            InitializeComponent();
            Icon = SystemIcons.Application;
            this.Width = (int)((Screen.FromControl(this).Bounds.Width * 0.75) - 20);
            this.Height = (int)((Screen.FromControl(this).Bounds.Height * 0.75) - 20);
            this.Left = (Screen.FromControl(this).Bounds.Width / 2) - (this.Width / 2);
            this.Top = (Screen.FromControl(this).Bounds.Height / 2) - (this.Height / 2 + 20);

            auto_add_sensors_ToolStripMenuItem.Text = "plug_dev";

            oxyChart = new OxyChart(ref chart);
        }

        private void about_programToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Текущая версия программы: " + Application.ProductVersion.ToString() + "\n" + "Разработчики: Р.Р. Галимов, Д.С. Васильев, Д.С. Гусельников." + "\n" + "\n" + "В данной программе используются следующие библиотеки: " + "\n" + "OxyPlot." + "\n" + "\n" + "Email: dmitrygdpwf@mail.ru", ActiveForm.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void all_cleartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sensors.Clear();
            oxyChart.DeleteAllShapes();

            UpdateFormState();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sensors.Clear();
            oxyChart.DeleteAllShapes();

            UpdateFormState();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void doToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void add_sensorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add_sensor_form form_add = new Add_sensor_form(ref Sensors);
            form_add.ShowDialog();
            RedrawAllChart();

            UpdateFormState();
        }

        private void add_wirelessToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void auto_add_sensors_ToolStripMenuItem_Click(object sender, EventArgs e)
        {


            UpdateFormState();
        }

        private void calcStatusLabel_Click(object sender, EventArgs e)
        {
            List<PointD> temp2 = new List<PointD>();
            PointD temp1 = Trilateration.FindPosition(Sensors, temp2);

            //out_x.Text = temp1.X.ToString();
            //out_y.Text = temp1.Y.ToString();

            if (temp1.X == -1 || temp1.Y == -1)
                return;

            for (int i = 0; i < temp2.Count - 1; i++)
            {
                oxyChart.DrawPoint(temp2[i].X, temp2[i].Y, OxyColors.Red);
                oxyChart.DrawLine(temp2[i].X, temp2[i].Y, temp2[i + 1].X, temp2[i + 1].Y, OxyColors.Red);
            }
            oxyChart.DrawPoint(temp2[temp2.Count - 1].X, temp2[temp2.Count - 1].Y, OxyColors.Red);
            oxyChart.DrawLine(temp2[temp2.Count - 1].X, temp2[temp2.Count - 1].Y, temp2[0].X, temp2[0].Y, OxyColors.Red);

            oxyChart.DrawPoint(temp1.X, temp1.Y, OxyColors.Green);

            UpdateFormState();
        }

        private void chart_MouseMove(object sender, MouseEventArgs e)
        {
            DataPoint pointMouseMove = oxyChart.oxyPlotModel.Axes[0].InverseTransform(e.X, e.Y, oxyChart.oxyPlotModel.Axes[1]);

            status_X.Text = Math.Round(pointMouseMove.X).ToString();
            status_Y.Text = Math.Round(pointMouseMove.Y).ToString();
        }

        private void display_current_coordinatesStripMenuItem_Click(object sender, EventArgs e)
        {
            display_current_coordinatesStripMenuItem.Checked = !display_current_coordinatesStripMenuItem.Checked;

            UpdateFormState();
        }
    }
}