﻿
namespace Emulator_Detect_Positioning_Wi_Fi
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.chart = new OxyPlot.WindowsForms.PlotView();
            this.status = new System.Windows.Forms.StatusStrip();
            this.separator_1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.calcStatusLabel = new System.Windows.Forms.ToolStripDropDownButton();
            this.separator_2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label_X_statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.status_X = new System.Windows.Forms.ToolStripStatusLabel();
            this.label_Y_statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.status_Y = new System.Windows.Forms.ToolStripStatusLabel();
            this.menu = new System.Windows.Forms.ToolStrip();
            this.menu_file = new System.Windows.Forms.ToolStripDropDownButton();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_edit = new System.Windows.Forms.ToolStripDropDownButton();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.doToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.all_cleartoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_view = new System.Windows.Forms.ToolStripDropDownButton();
            this.menu_add = new System.Windows.Forms.ToolStripDropDownButton();
            this.add_sensorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.add_wirelessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.auto_add_sensors_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_about = new System.Windows.Forms.ToolStripDropDownButton();
            this.about_programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.display_current_coordinatesStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.status.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart.Location = new System.Drawing.Point(0, 29);
            this.chart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart.Name = "chart";
            this.chart.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.chart.Size = new System.Drawing.Size(1196, 503);
            this.chart.TabIndex = 31;
            this.chart.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.chart.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.chart.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // status
            // 
            this.status.AutoSize = false;
            this.status.BackColor = System.Drawing.Color.White;
            this.status.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.separator_1,
            this.calcStatusLabel,
            this.separator_2,
            this.label_X_statusLabel,
            this.status_X,
            this.label_Y_statusLabel,
            this.status_Y});
            this.status.Location = new System.Drawing.Point(0, 534);
            this.status.Name = "status";
            this.status.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.status.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.status.Size = new System.Drawing.Size(1196, 40);
            this.status.TabIndex = 33;
            this.status.Text = "statusStrip1";
            // 
            // separator_1
            // 
            this.separator_1.Name = "separator_1";
            this.separator_1.Size = new System.Drawing.Size(544, 34);
            this.separator_1.Spring = true;
            // 
            // calcStatusLabel
            // 
            this.calcStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.calcStatusLabel.Enabled = false;
            this.calcStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("calcStatusLabel.Image")));
            this.calcStatusLabel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.calcStatusLabel.Name = "calcStatusLabel";
            this.calcStatusLabel.ShowDropDownArrow = false;
            this.calcStatusLabel.Size = new System.Drawing.Size(88, 38);
            this.calcStatusLabel.Text = "Рассчитать";
            this.calcStatusLabel.Click += new System.EventHandler(this.calcStatusLabel_Click);
            // 
            // separator_2
            // 
            this.separator_2.Name = "separator_2";
            this.separator_2.Size = new System.Drawing.Size(544, 34);
            this.separator_2.Spring = true;
            // 
            // label_X_statusLabel
            // 
            this.label_X_statusLabel.Name = "label_X_statusLabel";
            this.label_X_statusLabel.Size = new System.Drawing.Size(25, 34);
            this.label_X_statusLabel.Text = "X: ";
            this.label_X_statusLabel.Visible = false;
            // 
            // status_X
            // 
            this.status_X.Name = "status_X";
            this.status_X.Size = new System.Drawing.Size(17, 34);
            this.status_X.Text = "0";
            this.status_X.Visible = false;
            // 
            // label_Y_statusLabel
            // 
            this.label_Y_statusLabel.Name = "label_Y_statusLabel";
            this.label_Y_statusLabel.Size = new System.Drawing.Size(24, 34);
            this.label_Y_statusLabel.Text = "Y: ";
            this.label_Y_statusLabel.Visible = false;
            // 
            // status_Y
            // 
            this.status_Y.Name = "status_Y";
            this.status_Y.Size = new System.Drawing.Size(17, 34);
            this.status_Y.Text = "0";
            this.status_Y.Visible = false;
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.White;
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_file,
            this.menu_edit,
            this.menu_view,
            this.menu_add,
            this.menu_about});
            this.menu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1196, 27);
            this.menu.TabIndex = 34;
            this.menu.Text = "toolStrip1";
            // 
            // menu_file
            // 
            this.menu_file.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.toolStripSeparator4,
            this.openToolStripMenuItem1,
            this.toolStripSeparator1,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.closeToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
            this.menu_file.Image = ((System.Drawing.Image)(resources.GetObject("menu_file.Image")));
            this.menu_file.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menu_file.Name = "menu_file";
            this.menu_file.Size = new System.Drawing.Size(59, 24);
            this.menu_file.Text = "Файл";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.createToolStripMenuItem.Text = "Создать";
            this.createToolStripMenuItem.Visible = false;
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(221, 6);
            this.toolStripSeparator4.Visible = false;
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(224, 26);
            this.openToolStripMenuItem1.Text = "Открыть";
            this.openToolStripMenuItem1.Visible = false;
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(221, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.saveToolStripMenuItem.Text = "Сохранить";
            this.saveToolStripMenuItem.Visible = false;
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.saveAsToolStripMenuItem.Text = "Сохранить как";
            this.saveAsToolStripMenuItem.Visible = false;
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(221, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.closeToolStripMenuItem.Text = "Закрыть";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(221, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // menu_edit
            // 
            this.menu_edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.toolStripSeparator5,
            this.doToolStripMenuItem,
            this.toolStripSeparator8,
            this.all_cleartoolStripMenuItem});
            this.menu_edit.Image = ((System.Drawing.Image)(resources.GetObject("menu_edit.Image")));
            this.menu_edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menu_edit.Name = "menu_edit";
            this.menu_edit.Size = new System.Drawing.Size(74, 24);
            this.menu_edit.Text = "Правка";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.undoToolStripMenuItem.Text = "Отменить";
            this.undoToolStripMenuItem.Visible = false;
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(206, 6);
            this.toolStripSeparator5.Visible = false;
            // 
            // doToolStripMenuItem
            // 
            this.doToolStripMenuItem.Enabled = false;
            this.doToolStripMenuItem.Name = "doToolStripMenuItem";
            this.doToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.doToolStripMenuItem.Text = "Вернуть";
            this.doToolStripMenuItem.Visible = false;
            this.doToolStripMenuItem.Click += new System.EventHandler(this.doToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(206, 6);
            this.toolStripSeparator8.Visible = false;
            // 
            // all_cleartoolStripMenuItem
            // 
            this.all_cleartoolStripMenuItem.Enabled = false;
            this.all_cleartoolStripMenuItem.Name = "all_cleartoolStripMenuItem";
            this.all_cleartoolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.all_cleartoolStripMenuItem.Text = "Очистить график";
            this.all_cleartoolStripMenuItem.Click += new System.EventHandler(this.all_cleartoolStripMenuItem_Click);
            // 
            // menu_view
            // 
            this.menu_view.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_view.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.display_current_coordinatesStripMenuItem});
            this.menu_view.Image = ((System.Drawing.Image)(resources.GetObject("menu_view.Image")));
            this.menu_view.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menu_view.Name = "menu_view";
            this.menu_view.Size = new System.Drawing.Size(49, 24);
            this.menu_view.Text = "Вид";
            // 
            // menu_add
            // 
            this.menu_add.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_add.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add_sensorToolStripMenuItem,
            this.toolStripSeparator6,
            this.add_wirelessToolStripMenuItem,
            this.toolStripSeparator7,
            this.auto_add_sensors_ToolStripMenuItem});
            this.menu_add.Image = ((System.Drawing.Image)(resources.GetObject("menu_add.Image")));
            this.menu_add.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menu_add.Name = "menu_add";
            this.menu_add.Size = new System.Drawing.Size(90, 24);
            this.menu_add.Text = "Добавить";
            // 
            // add_sensorToolStripMenuItem
            // 
            this.add_sensorToolStripMenuItem.Name = "add_sensorToolStripMenuItem";
            this.add_sensorToolStripMenuItem.Size = new System.Drawing.Size(508, 26);
            this.add_sensorToolStripMenuItem.Text = "Сенсор";
            this.add_sensorToolStripMenuItem.Click += new System.EventHandler(this.add_sensorToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(505, 6);
            // 
            // add_wirelessToolStripMenuItem
            // 
            this.add_wirelessToolStripMenuItem.Name = "add_wirelessToolStripMenuItem";
            this.add_wirelessToolStripMenuItem.Size = new System.Drawing.Size(508, 26);
            this.add_wirelessToolStripMenuItem.Text = "Источник беспроводного сигнала";
            this.add_wirelessToolStripMenuItem.Click += new System.EventHandler(this.add_wirelessToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(505, 6);
            this.toolStripSeparator7.Visible = false;
            // 
            // auto_add_sensors_ToolStripMenuItem
            // 
            this.auto_add_sensors_ToolStripMenuItem.Enabled = false;
            this.auto_add_sensors_ToolStripMenuItem.Name = "auto_add_sensors_ToolStripMenuItem";
            this.auto_add_sensors_ToolStripMenuItem.Size = new System.Drawing.Size(508, 26);
            this.auto_add_sensors_ToolStripMenuItem.Text = "Сенсоры в автоматическом режиме по беспроводной сети";
            this.auto_add_sensors_ToolStripMenuItem.Visible = false;
            this.auto_add_sensors_ToolStripMenuItem.Click += new System.EventHandler(this.auto_add_sensors_ToolStripMenuItem_Click);
            // 
            // menu_about
            // 
            this.menu_about.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_about.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.about_programToolStripMenuItem});
            this.menu_about.Image = ((System.Drawing.Image)(resources.GetObject("menu_about.Image")));
            this.menu_about.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menu_about.Name = "menu_about";
            this.menu_about.Size = new System.Drawing.Size(81, 24);
            this.menu_about.Text = "Справка";
            // 
            // about_programToolStripMenuItem
            // 
            this.about_programToolStripMenuItem.Name = "about_programToolStripMenuItem";
            this.about_programToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.about_programToolStripMenuItem.Text = "О программе";
            this.about_programToolStripMenuItem.Click += new System.EventHandler(this.about_programToolStripMenuItem_Click);
            // 
            // display_current_coordinatesStripMenuItem
            // 
            this.display_current_coordinatesStripMenuItem.Name = "display_current_coordinatesStripMenuItem";
            this.display_current_coordinatesStripMenuItem.Size = new System.Drawing.Size(327, 26);
            this.display_current_coordinatesStripMenuItem.Text = "Отображать текущие координаты";
            this.display_current_coordinatesStripMenuItem.Click += new System.EventHandler(this.display_current_coordinatesStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 574);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.status);
            this.Controls.Add(this.chart);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.Text = "Эмулятор обнаружения позиции источника беспроводного сигнала";
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private OxyPlot.WindowsForms.PlotView chart;
        private System.Windows.Forms.StatusStrip status;
        private System.Windows.Forms.ToolStrip menu;
        private System.Windows.Forms.ToolStripDropDownButton menu_add;
        private System.Windows.Forms.ToolStripDropDownButton menu_about;
        private System.Windows.Forms.ToolStripMenuItem about_programToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel separator_1;
        private System.Windows.Forms.ToolStripStatusLabel label_X_statusLabel;
        private System.Windows.Forms.ToolStripStatusLabel status_X;
        private System.Windows.Forms.ToolStripStatusLabel label_Y_statusLabel;
        private System.Windows.Forms.ToolStripStatusLabel status_Y;
        private System.Windows.Forms.ToolStripStatusLabel separator_2;
        private System.Windows.Forms.ToolStripDropDownButton calcStatusLabel;
        private System.Windows.Forms.ToolStripDropDownButton menu_file;
        private System.Windows.Forms.ToolStripDropDownButton menu_edit;
        private System.Windows.Forms.ToolStripDropDownButton menu_view;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem doToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem add_sensorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem add_wirelessToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem auto_add_sensors_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem all_cleartoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem display_current_coordinatesStripMenuItem;
    }
}