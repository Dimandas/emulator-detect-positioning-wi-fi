﻿using System;
using System.Collections.Generic;


namespace Emulator_Detect_Positioning_Wi_Fi.Source.Trilateration
{
    public class Sensor
    {
        public enum TypeSensor
        {
            Free_space_path_loss,
            Friis_transmission_equation,
            Time,
            None
        }

        public enum WorkMode
        {
            None
        }

        public TypeSensor kind_sensor = TypeSensor.None;

        public string name = "";
        public bool enabled = true;
        public WorkMode workMode = WorkMode.None;

        public double X = 0;
        public double Y = 0;
        public double Z = 0;
        public double distance = 0;

        public double frequency = 0;
        public double PWR = 0;

        public double time_ns = 0;

        public Sensor(double _x, double _y, double _PWR, double _frequency, string _name = "", double _z = 0)
        {
            name = _name;

            X = _x;
            Y = _y;
            Z = _z;

            PWR = _PWR;
            frequency = _frequency;

            distance = Trilateration.CalculateDistanceAttenuationFSPL(PWR, frequency);

            kind_sensor = TypeSensor.Free_space_path_loss;
        }

        public Sensor(double _x, double _y, double _time_ns, string _name = "", double _z = 0)
        {
            name = _name;

            X = _x;
            Y = _y;
            Z = _z;

            time_ns = _time_ns;

            // ToDo

            //Distance = Trilateration.CalculateDistanceAttenuationTime(PWR, frequency);

            kind_sensor = TypeSensor.Time;
        }

        public Sensor(double _x, double _y, TypeSensor type, string _name = "", double _z = 0)
        {
            name = _name;

            X = _x;
            Y = _y;
            Z = _z;

            distance = 0;

            kind_sensor = type;
        }
    }

    public struct PointD
    {
        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X;
        public double Y;
    }

    public static class Trilateration
    {
        public static double CalculateDistanceAttenuationFSPL(double ipwr, double freq)
        {
            double exp1 = (double)((27.55 - (20 * Math.Log10(freq / 1000)) + Math.Abs(ipwr)) / 20.0);
            double answer = (double)Math.Pow(10.0, exp1);

            return answer;
        }

        public static double CalculateDistanceAttenuationFriis(double ipwr, double freq)
        {
            // ToDo

            return -1;
        }

        public static double CalculateDistanceAttenuationTime(double ipwr, double freq)
        {
            // ToDo

            return -1;
        }

        public static double FindDekartDistance(double x1, double y1, double x2, double y2)
        {
            return (double)Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }

        public static List<PointD> FindIntersectionCircles(double x1, double y1, double r1, double x2, double y2, double r2, double distance)
        {
            List<PointD> result = new List<PointD>() { };

            if (distance == 0 && r1 == r2)
            {
                //throw std::runtime_error("there are no solutions: circles are identical (are one and the same circumference)");
                return result;
            }

            if (distance > r1 + r2)
            {
                //throw std::runtime_error("there are no solutions: the circles are separate");
                return result;
            }

            if (distance < Math.Abs(r1 - r2))
            {
                //throw std::runtime_error("there are no solutions: because one circle is inside the other");
                return result;
            }

            double a = (Math.Pow(r1, 2) - Math.Pow(r2, 2) + Math.Pow(distance, 2)) / (2 * distance);

            double h = Math.Sqrt(Math.Pow(r1, 2) - Math.Pow(a, 2));

            double x3 = x1 + a * (x2 - x1) / distance;
            double y3 = y1 + a * (y2 - y1) / distance;

            double x4 = x3 + h * (y2 - y1) / distance;
            double y4 = y3 - h * (x2 - x1) / distance;

            double x5 = x3 - h * (y2 - y1) / distance;
            double y5 = y3 + h * (x2 - x1) / distance;

            result.Add(new PointD(x4, y4));
            result.Add(new PointD(x5, y5));

            return result;
        }

        public static PointD FindPosition(List<Sensor> input, List<PointD> outIntermediatePoints = null)
        {
            List<PointD> intersection = new List<PointD>() { };

            for (int i1 = 0; i1 < input.Count; i1++)
            {
                for (int i2 = i1; i2 < input.Count; i2++)
                {
                    List<PointD> temp1 = FindIntersectionCircles(input[i1].X, input[i1].Y, input[i1].distance, input[i2].X, input[i2].Y, input[i2].distance, FindDekartDistance(input[i1].X, input[i1].Y, input[i2].X, input[i2].Y));
                    intersection.AddRange(temp1);
                }
            }

            for (int i1 = 0; i1 < intersection.Count; i1++)
            {
                for (int i2 = 0; i2 < input.Count; i2++)
                {
                    double tempDistance = FindDekartDistance(intersection[i1].X, intersection[i1].Y, input[i2].X, input[i2].Y);

                    if (tempDistance > input[i2].distance + 1)
                    {
                        intersection.RemoveAt(i1);
                        i1 = i1 - 1;

                        break;
                    }
                }
            }

            if (outIntermediatePoints != null)
            {
                outIntermediatePoints.Clear();
                outIntermediatePoints.AddRange(intersection);
            }

            return FindCenterPolygon(intersection);
        }

        public static PointD FindCenterPolygon(List<PointD> inputPoints)
        {
            if (inputPoints.Count <= 0)
            {
                return new PointD(-1, -1);
            }

            double sumX = 0;
            double sumY = 0;

            for (int i = 0; i < inputPoints.Count; i++)
            {
                sumX = sumX + inputPoints[i].X;
                sumY = sumY + inputPoints[i].Y;
            }

            sumX = sumX / inputPoints.Count;
            sumY = sumY / inputPoints.Count;

            return new PointD(sumX, sumY);
        }
    }
}